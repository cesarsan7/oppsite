﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OppSite.Models
{
    public class ReportNacionalizacion
    {
        public int consecutivo { get; set; }
        public DateTime? FechaAuditoria { get; set; }
        public string Autorizo { get; set; }
        public string NombreUsuario { get; set; }
        
        public string computador { get; set; }

        public string Motivo { get; set; }
        public string DetalleAuditoria { get; set; }
        public string Razon { get; set; }        
        public string empresa { get; set; }


    }
}