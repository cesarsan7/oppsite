﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OppSite.Models
{
    public class ReportDestarados
    {
        public string mn { get; set; }
        public string cliente { get; set; }
        public string destino { get; set; }
        public string producto { get; set; }
        public string concepto { get; set; }
        public string bodega { get; set; }
        public string transportadora { get; set; }
        public int? escotilla { get; set; }
        public string placa { get; set; }
        public int? consecutivo { get; set; }
        public string visita { get; set; }
        public string orden { get; set; }
        public int? tara { get; set; }
        public int? destara { get; set; }
        public int? neto { get; set; }
        public DateTime? reservaT { get; set; }
        public DateTime? enturnado { get; set; }
        public DateTime? radicado { get; set; }
        public DateTime? tarado { get; set; }
        public DateTime? destarado { get; set; }
        public string equipoDesc { get; set; }
        public string basTara { get; set; }
        public string basDestara { get; set; }
        public DateTime? ingresaSprb { get; set; }
        public DateTime? salidaSprb { get; set; }

        public string empresa { get; set; }

        public string Compartido { get; set; }

    }
}