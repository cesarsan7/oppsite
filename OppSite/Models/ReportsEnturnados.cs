﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OppSite.Models
{
    public class ReportsEnturnados
    {
        public string mn { get; set; }
        public string cliente { get; set; }
        public string destino { get; set; }
        public string producto { get; set; }
        public string transportadora { get; set; }
        public string placa { get; set; }
        public string orden { get; set; }
        public string carga_en { get; set; }
        public string reservaT { get; set; }
        public DateTime? enturnado { get; set; }
        public string sitioEnturnamiento { get; set; }
        public DateTime? citaSpb { get; set; }

        public string empresa { get; set; }
        public DateTime? reservaTFecha { get; set; }
        public string  nombreConductor { get; set; }
        public string cedula  { get; set; }
        public string numeroDeposito { get; set; }
        public string ddi_ordens  { get; set; }

        public string sitioCargue { get; set; }
        public string Bodega { get; set; }

        public DateTime? fechaInicial { get; set; }
        public DateTime? fechaFinal { get; set; }

        public string Idscdad { get; set; }
        public string Remolque { get; set; }

    }
}