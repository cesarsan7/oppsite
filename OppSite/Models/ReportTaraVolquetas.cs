﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OppSite.Models
{
    public class ReportTaraVolquetas
    {
        public string transportadora { get; set; }
        public string placa { get; set; }
        public DateTime? fecha { get; set; }
        public string cedula { get; set; }
        public string conductor { get; set; }
        public int? tara { get; set; }
        public int? pesadas { get; set; }
        public string empresa { get; set; }
    }
}