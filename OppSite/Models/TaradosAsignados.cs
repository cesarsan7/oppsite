﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OppSite.Models
{
    public class TaradosAsignados
    {
       public  string mn                    { get; set;  }
        public string placa                 { get; set;  }
        public string producto              { get; set;  }
        public string bodega                { get; set;  }
        public DateTime? tarado              { get; set;  }
        public DateTime? destarado           { get; set;  }
        public DateTime? reservaTurno        { get; set;  }
        public string bodegaAsignada        { get; set;  }
        public string observacion { get; set; }
        public  string fechaCreacion { get; set; }
        public DateTime? fechaCreacion2 { get; set; }
        public string userId { get; set; }

        public string orden { get; set; }

        public string empresa { get; set; }

        public string escotilla { get; set; }

        public int? pesoNeto { get; set; }
    }
}