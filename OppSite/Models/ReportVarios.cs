﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OppSite.Models
{
    public class ReportVarios
    {
        public string deposito { get; set; }
        public string concepto { get; set; }
        public int? escotilla { get; set; }
        public string mn { get; set; }
        public string producto { get; set; }
        public string bodega { get; set; }
        public string placa { get; set; }
        public int? consecutivo { get; set; }
        public string ordenInterna { get; set; }
        public DateTime? entrada { get; set; }
        public DateTime? salida { get; set; }
        public int? tara { get; set; }
        public int? bruto { get; set; }
        public int? neto { get; set; }
        public string equipoDescargue { get; set; }
        public string bas_tara { get; set; }
        public string bas_destara { get; set; }
        public string observaciones { get; set; }
        public string tiCncpto { get; set; }

        public string empresa { get; set; }


    }
}