﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OppSite.Models
{
    public class ReportDestaradosAsignados
    {
        public string mn { get; set; }
        public string cliente { get; set; }
        public string destino { get; set; }
        public string producto { get; set; }
        public string concepto { get; set; }
        public string bodega { get; set; }
        public string transportadora { get; set; }
        public int? escotilla { get; set; }
        public string placa { get; set; }
        public int? consecutivo { get; set; }
        public string visita { get; set; }
        public string orden { get; set; }
        public int? tara { get; set; }
         

        public string bodegaAsignada { get; set; }
        public string descripcion { get; set; }

        public string fechaAsignacion { get; set; }
    }

}