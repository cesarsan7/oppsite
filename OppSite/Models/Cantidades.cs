﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OppSite.Models
{
    public class Cantidades
    {

        public int cantidadEnturnados { get; set; }
        public int cantDestarados { get; set; }
        public int cantMuelle { get; set; }
        public int cantRadicados { get; set; }
        public int cantReservaTurnos { get; set; }
        public int cantTarados { get; set; }
        public int cantTaraVolquetas { get; set; }
        public int cantVarios { get; set; }
    }
}