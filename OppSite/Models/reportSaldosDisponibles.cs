﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OppSite.Models
{
    public class saldosDisponibles
    {

        public DateTime? fecha { get; set; }
        public string hora { get; set; }
        public string deposito { get; set; }
        public string mn { get; set; }
        public string cliente { get; set; }
        public string producto { get; set; }
        public int? BL { get; set; }
        public int? nacionalizado { get; set; }
        public int? despacho { get; set; }
        public int? saldoAlBl { get; set; }
        public int? saldoAlNac { get; set; }

        public string empresa { get; set; }
    }
}