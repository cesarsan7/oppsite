﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OppSite.Models
{
    public class ReportTarados
    {
        public string mn { get; set; }
        public string producto { get; set; }
        public string cliente { get; set; }
        public string transportadora { get; set; }
        public string destino { get; set; }
        public string concepto { get; set; }
        public string bodega { get; set; }
        public string placa { get; set; }
        public string orden { get; set; }
        public int tara { get; set; }
        public DateTime? reservaT { get; set; }
        public DateTime? enturnado { get; set; }
        public DateTime? radicado { get; set; }
        public DateTime? ingresaSprb { get; set; }
        public DateTime? tarado { get; set; }
        public string empresa { get; set; }

        public int compartido { get; set; }
        public string horaReservaT { get; set; }
        public string HoraEnturnado { get; set; }
        public string HoraRadicado { get; set; }
        public string HoraTarado { get; set; }

    }
}