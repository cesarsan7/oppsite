﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OppSite.Models
{
    public class ReportReservaTurno
    {
        public string mn { get; set; }
        public string cliente { get; set; }
        public string destino { get; set; }
        public string producto { get; set; }
        public string transportadora { get; set; }
        public string placa { get; set; }
        public string r { get; set; }
        public string orden { get; set; }
        public string conductor { get; set; }
        public DateTime? reservaT { get; set; }
        public DateTime? esperando { get; set; }
        public DateTime? horaEstimadaCita { get; set; }
        public string empresa { get; set; }

        public string duracion { get; set; }
    }
}