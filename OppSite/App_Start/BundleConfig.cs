﻿using System.Web;
using System.Web.Optimization;

namespace OppSite
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            //bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
            //            "~/Scripts/jquery-{version}.js"));

            //// Use the development version of Modernizr to develop with and learn from. Then, when you're
           //// ready for production, use the build tool at o pick only the tests you need.
            //bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
            //            "~/Scripts/modernizr-*"));



            bundles.Add(new StyleBundle("~/content/css").Include(
                        "~/Content/vendors/bootstrap/dist/css/.min.css",
                        "~/Content/vendors/font-awesome/css/font-awesome.min.css",
                        "~/Content/vendors/nprogress/nprogress.css",
                        "~/Content/vendors/iCheck/skins/flat/green.css",
                        "~/Content/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css",
                        "~/Content/vendors/jqvmap/dist/jqvmap.min.css",
                        "~/Content/vendors/bootstrap-daterangepicker/daterangepicker.css",
                        "~/Content/build/css/custom.min.css"
                     
                       
                      ));


            bundles.Add(new ScriptBundle("~/content/bootstrap").Include(
             "~/Content/vendors/bootstrap/dist/js/bootstrap.min.js",
             "~/Content/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js",
              "~/Content/build/js/custom.min.js"));

            bundles.Add(new ScriptBundle("~/content/jquery").Include(
                        "~/Content/vendors/jquery/dist/jquery.min.js",
                        "~/Content/docs/css/jquery.dataTables.min.css",
                        "~/Content/vendors/datatables.net/js/jquery.dataTables.min.js",
                   
                        "~/Content/vendors/nprogress/nprogress.js",
                        "~/Content/vendors/fastclick/lib/fastclick.js",       //<!-- FastClick -->  
                            "~/Content/vendors/nprogress/nprogress.js",       //  <!-- NProgress -->   
                            "~/Content/vendors/Chart.js/dist/Chart.min.js",
                            "~/Content/vendors/gauge.js/dist/gauge.min.js",    //<!-- gauge.js -->
                            "~/Content/vendors/iCheck/icheck.min.js",     //   <!-- iCheck -->

                            "~/Content/vendors/skycons/skycons.js",   //    <!-- Skycons -->
                                                                      // <!-- Flot -->
                            "~/Content/vendors/Flot/jquery.flot.js",
                            "~/Content/vendors/Flot/jquery.flot.pie.js",
                            "~/Content/vendors/Flot/jquery.flot.time.js",
                            "~/Content/vendors/Flot/jquery.flot.stack.js",
                            "~/Content/vendors/Flot/jquery.flot.resize.js",
                            //<!-- Flot plugins -->
                            "~/Content/vendors/flot.orderbars/js/jquery.flot.orderBars.js",
                            "~/Content/vendors/flot-spline/js/jquery.flot.spline.min.js",
                            "~/Content/vendors/flot.curvedlines/curvedLines.js",
                            //<!-- DateJS -->
                            "~/Content/vendors/DateJS/build/date.js",
                            //<!-- JQVMap -->
                            "~/Content/vendors/jqvmap/dist/jquery.vmap.js",
                            "~/Content/vendors/jqvmap/dist/maps/jquery.vmap.world.js",
                            "~/Content/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js",
                            // <!-- bootstrap-daterangepicker 
                            "~/Content/vendors/moment/min/moment.min.js",
                            "~/Content/vendors/bootstrap-daterangepicker/daterangepicker.js"
                            
                        // <!-- Custom Theme Scripts -->


                        ));

   

        }
    }
}
