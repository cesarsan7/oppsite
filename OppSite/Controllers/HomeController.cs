﻿using OppSite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace OppSite.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult cantidades()
        {
            ViewBag.Title = "Opp Gramenes Inicio";
           // ViewBag.totalRadicados = 11;
            Cantidades cantidades = new Cantidades();

            using (var cant = new HttpClient())
            {
                cant.BaseAddress = new Uri("http://localhost:1202/api/Reports/Cantidades");

                var responseTask = cant.GetAsync("Cantidades");
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<Cantidades>();
                    readTask.Wait();

                    cantidades = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..

                    cantidades = new Cantidades();
                   
                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }

            }
            //ViewData["cantDestarados"] = cantidades.cantDestarados;
            ViewBag.cantidadEnturnados = cantidades.cantidadEnturnados;
            ViewBag.cantDestarados = cantidades.cantDestarados;
            ViewBag.cantMuelle = cantidades.cantMuelle;
            ViewBag.cantRadicados = cantidades.cantRadicados;
            ViewBag.cantReservaTurnos = cantidades.cantReservaTurnos;
            ViewBag.cantTarados = cantidades.cantTarados;
            ViewBag.cantTaraVolquetas = cantidades.cantTaraVolquetas;
            ViewBag.cantVarios = cantidades.cantVarios;
            return View();
        } 

        public ActionResult Index()
        {
            return View();
        }
    }
}
