﻿using OppSite.Data;
using OppSite.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OppSite.Controllers
{
    public class ReportsController : ApiController
    {



        private venus_oppEntities db = new venus_oppEntities();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Route("api/Reports/Enturnados")]
        [HttpGet]
        public IEnumerable<ReportsEnturnados> Enturnados()
        {

            return (from item in db.vw_enturnados

                    select new ReportsEnturnados
                    {
                        mn = item.MN,
                        cliente = item.CLIENTE,
                        destino = item.DESTINO,
                        producto = item.PRODUCTO,
                        transportadora = item.TRANSPORTADORA,
                        placa = item.PLACA,
                        carga_en = item.CARGA_EN,
                        citaSpb = item.CIta_spb,
                        enturnado = item.ENTURNADO,
                        orden = item.ORDEN,
                        reservaT = item.RESERVA_TURNO != null ? item.RESERVA_TURNO.Value.ToString() : string.Empty,
                        sitioEnturnamiento = item.SITIO_ENTURNAMIENTO,
                        empresa = item.empresa,
                        reservaTFecha = item.RESERVA_TURNO,
                        sitioCargue = item.pd_stio_crgue,
                        Bodega = item.pd_bdga,
                        fechaInicial = item.pd_fcha_incio,
                        fechaFinal = item.pd_fcha_fnal,
                        Idscdad = item.or_id_scdad,
                        Remolque = item.or_rmlque

                    }

                    );

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Route("api/Reports/Enturnados/{fechaInicial}/{fechaFinal}")]
        [HttpGet]
        public IEnumerable<ReportsEnturnados> Enturnados(string fechaInicial, string fechaFinal)
        {

            DateTime fInicial = DateTime.ParseExact(fechaInicial, "yyyy-MM-dd", System.Globalization.CultureInfo.CurrentCulture, System.Globalization.DateTimeStyles.None);
            DateTime fFinal = DateTime.ParseExact(fechaFinal, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
            fFinal = fFinal.AddDays(1);

            IEnumerable<ReportsEnturnados> list = (from item in db.vw_enturnados.Where(x => x.ENTURNADO >= fInicial && x.ENTURNADO <= fFinal)
                                                   select new ReportsEnturnados
                                                   {
                                                       mn = item.MN,
                                                       cliente = item.CLIENTE,
                                                       destino = item.DESTINO,
                                                       producto = item.PRODUCTO,
                                                       transportadora = item.TRANSPORTADORA,
                                                       placa = item.PLACA,
                                                       carga_en = item.CARGA_EN,
                                                       citaSpb = item.CIta_spb,
                                                       enturnado = item.ENTURNADO,
                                                       orden = item.ORDEN,
                                                       reservaT = item.RESERVA_TURNO != null ? item.RESERVA_TURNO.Value.ToString() : string.Empty,
                                                       sitioEnturnamiento = item.SITIO_ENTURNAMIENTO,
                                                       empresa = item.empresa,
                                                       reservaTFecha = item.RESERVA_TURNO,
                                                       nombreConductor = item.NOMBRE,
                                                       cedula = item.CEDULA,
                                                       numeroDeposito = item.numero_deposito,
                                                       ddi_ordens = item.ddi_ordens,
                                                       sitioCargue = item.pd_stio_crgue,
                                                       Bodega = item.pd_bdga,
                                                       fechaInicial = item.pd_fcha_incio,
                                                       fechaFinal = item.pd_fcha_fnal,
                                                       Idscdad = item.or_id_scdad,
                                                       Remolque = item.or_rmlque


                                                   }

                    );
            return list;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Route("api/Reports/ReservaTurnos")]
        [HttpGet]
        public IEnumerable<ReportReservaTurno> ReservaTurnos()
        {

            return (from item in db.vw_reserva_turnos

                    select new ReportReservaTurno
                    {
                        mn = item.MN,
                        cliente = item.CLIENTE,
                        destino = item.DESTINO,
                        producto = item.PRODUCTO,
                        transportadora = item.TRANSPORTADORA,
                        placa = item.PLACA,
                        r = item.R,
                        orden = item.ORDEN,
                        conductor = item.CONDUCTOR,
                        reservaT = item.RESERVA_TURNO,
                        esperando = item.ESPERANDO,
                        horaEstimadaCita = item.Hora_estimada_Cita,
                        empresa = item.empresa
                    }

                    );

        }

        [Route("api/Reports/ReservaTurnos/{fechaInicial}/{fechaFinal}")]
        [HttpGet]
        public IEnumerable<ReportReservaTurno> ReservaTurnos(string fechaInicial, string fechaFinal)
        {

            DateTime fInicial = DateTime.ParseExact(fechaInicial, "yyyy-MM-dd", System.Globalization.CultureInfo.CurrentCulture, System.Globalization.DateTimeStyles.None);
            DateTime fFinal = DateTime.ParseExact(fechaFinal, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
            fFinal = fFinal.AddDays(1);

            IEnumerable<ReportReservaTurno> list = (from item in db.vw_reserva_turnos.Where(x => x.RESERVA_TURNO >= fInicial && x.RESERVA_TURNO <= fFinal)

                                                    select new ReportReservaTurno
                                                    {
                                                        mn = item.MN,
                                                        cliente = item.CLIENTE,
                                                        destino = item.DESTINO,
                                                        producto = item.PRODUCTO,
                                                        transportadora = item.TRANSPORTADORA,
                                                        placa = item.PLACA,
                                                        r = item.R,
                                                        orden = item.ORDEN,
                                                        conductor = item.CONDUCTOR,
                                                        reservaT = item.RESERVA_TURNO,
                                                        //  esperando = item.ESPERANDO,
                                                        horaEstimadaCita = item.Hora_estimada_Cita,
                                                        empresa = item.empresa,
                                                        duracion = item.diferencia
                                                    }
                    );
            return list;

        }

        [Route("api/Reports/Radicados")]
        [HttpGet]
        public IEnumerable<ReportRadicados> Radicados()
        {


            var valor = db.vw_radicados.Where(x => x.ORDEN.Length > 0);
            return (from item in db.vw_radicados

                    select new ReportRadicados
                    {
                        mn = item.MN,
                        cliente = item.CLIENTE,
                        destino = item.DESTINO,
                        producto = item.PRODUCTO,
                        transportadora = item.TRANSPORTADORA,
                        placa = item.PLACA,
                        orden = item.ORDEN,
                        sitioEnturnamiento = item.SITIO_ENTURNAMIENTO,
                        citaSpb = item.CIta_spb,
                        enturnado = item.ENTURNADO,
                        radicado = item.RADICADO,
                        reservaT = item.RESERVA_TURNO,
                        tiempo = item.TIEMPO,
                        empresa = item.empresa,
                        sitioCargue = item.pd_stio_crgue,
                        Bodega = item.pd_bdga,
                        fechaInicial =item.pd_fcha_incio,
                        fechaFinal =item.pd_fcha_fnal,
                        Idscdad  =item.or_id_scdad ,
                        Remolque =item.or_rmlque

                    
                    }

                    );
        }

        [Route("api/Reports/Radicados/{fechaInicial}/{fechaFinal}")]
        [HttpGet]
        public IEnumerable<ReportRadicados> Radicados(string fechaInicial, string fechaFinal)
        {

            DateTime fInicial = DateTime.ParseExact(fechaInicial, "yyyy-MM-dd", System.Globalization.CultureInfo.CurrentCulture, System.Globalization.DateTimeStyles.None);
            DateTime fFinal = DateTime.ParseExact(fechaFinal, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
            fFinal = fFinal.AddDays(1);

            IEnumerable<ReportRadicados> list = (from item in db.vw_radicados.Where(x => x.ORDEN.Length > 0 && x.RADICADO >= fInicial && x.RADICADO <= fFinal)

                                                 select new ReportRadicados
                                                 {
                                                     mn = item.MN,
                                                     cliente = item.CLIENTE,
                                                     destino = item.DESTINO,
                                                     producto = item.PRODUCTO,
                                                     transportadora = item.TRANSPORTADORA,
                                                     placa = item.PLACA,
                                                     orden = item.ORDEN,
                                                     sitioEnturnamiento = item.SITIO_ENTURNAMIENTO,
                                                     citaSpb = item.CIta_spb,
                                                     enturnado = item.ENTURNADO,
                                                     radicado = item.RADICADO,
                                                     reservaT = item.RESERVA_TURNO,
                                                     tiempo = item.TIEMPO,
                                                     empresa = item.empresa,
                                                     sitioCargue = item.pd_stio_crgue,
                                                     Bodega = item.pd_bdga,
                                                     fechaInicial = item.pd_fcha_incio,
                                                     fechaFinal = item.pd_fcha_fnal,
                                                     Idscdad = item.or_id_scdad,
                                                     Remolque = item.or_rmlque
                                                 }

                    );
            return list;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Route("api/Reports/Tarados")]
        [HttpGet]
        public IQueryable<ReportTarados> Tarados()
        {

            // var fecha = lst.FirstOrDefault().tarado;
            return (from item in db.vw_tarados

                    select new ReportTarados
                    {
                        mn = item.MN,
                        cliente = item.CLIENTE,
                        destino = item.DESTINO,
                        producto = item.PRODUCTO,
                        transportadora = item.TRANSPORTADORA,
                        placa = item.PLACA,
                        orden = item.ORDEN,
                        concepto = item.CONCEPTO,
                        bodega = item.BODEGA,
                        tarado = item.TARADO,
                        ingresaSprb = item.INGRESA_SPRB,
                        tara = item.TARA != null ? item.TARA.Value : -1,
                        enturnado = item.ENTURNADO,
                        radicado = item.RADICADO,
                        reservaT = item.RESERVA_TURNO,
                        empresa = item.empresa,
                        compartido = item.tr_cmprtdo

                    }

                    );
        }


        [Route("api/Reports/Tarados/{fechaInicial}/{fechaFinal}")]
        [HttpGet]
        public IEnumerable<ReportTarados> Tarados(string fechaInicial, string fechaFinal)
        {


            DateTime fInicial = DateTime.ParseExact(fechaInicial, "yyyy-MM-dd", System.Globalization.CultureInfo.CurrentCulture, System.Globalization.DateTimeStyles.None);
            DateTime fFinal = DateTime.ParseExact(fechaFinal, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
            fFinal = fFinal.AddDays(1);

            IEnumerable<ReportTarados> list = (from item in db.vw_tarados.Where(x => SqlFunctions.DateAdd("day", 0, x.tr_fcha_entrda) >= fInicial && SqlFunctions.DateAdd("day", 0, x.tr_fcha_entrda) <= fFinal)

                                               select new ReportTarados
                                               {
                                                   mn = item.MN,
                                                   cliente = item.CLIENTE,
                                                   destino = item.DESTINO,
                                                   producto = item.PRODUCTO,
                                                   transportadora = item.TRANSPORTADORA,
                                                   placa = item.PLACA,
                                                   orden = item.ORDEN,
                                                   concepto = item.CONCEPTO,
                                                   bodega = item.BODEGA,
                                                   tarado = item.TARADO,
                                                   ingresaSprb = item.INGRESA_SPRB,
                                                   tara = item.TARA != null ? item.TARA.Value : -1,
                                                   enturnado = item.ENTURNADO,
                                                   radicado = item.RADICADO,
                                                   reservaT = item.RESERVA_TURNO,
                                                   empresa = item.empresa,
                                                   compartido = item.tr_cmprtdo,
                                                   horaReservaT = item.HoraReservaTurno,
                                                   HoraEnturnado = item.HoraEnturnado,
                                                   HoraRadicado = item.HoraRadicado,
                                                   HoraTarado = item.HoraTarado

                                               }

                    );
            return list;
        }


        [Route("api/Reports/TaradosPorVerificar/{fechaInicial}/{fechaFinal}")]
        [HttpGet]
        public IEnumerable<ReportTarados> TaradosPorVerificar(string fechaInicial, string fechaFinal)
        {


            DateTime fInicial = DateTime.ParseExact(fechaInicial, "yyyy-MM-dd", System.Globalization.CultureInfo.CurrentCulture, System.Globalization.DateTimeStyles.None);
            DateTime fFinal = DateTime.ParseExact(fechaFinal, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
            fFinal = fFinal.AddDays(1);

            IEnumerable<ReportTarados> list = (from item in db.vw_tarados_por_verificar.Where(x => SqlFunctions.DateAdd("day", 0, x.tr_fcha_entrda) >= fInicial && SqlFunctions.DateAdd("day", 0, x.tr_fcha_entrda) <= fFinal)

                                               select new ReportTarados
                                               {
                                                   mn = item.MN,
                                                   cliente = item.CLIENTE,
                                                   destino = item.DESTINO,
                                                   producto = item.PRODUCTO,
                                                   transportadora = item.TRANSPORTADORA,
                                                   placa = item.PLACA,
                                                   orden = item.ORDEN,
                                                   concepto = item.CONCEPTO,
                                                   bodega = item.BODEGA,
                                                   tarado = item.TARADO,
                                                   ingresaSprb = item.INGRESA_SPRB,
                                                   tara = item.TARA != null ? item.TARA.Value : -1,
                                                   enturnado = item.ENTURNADO,
                                                   radicado = item.RADICADO,
                                                   reservaT = item.RESERVA_TURNO,
                                                   empresa = item.empresa,
                                                   compartido = item.tr_cmprtdo

                                               }

                    );
            return list;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Route("api/Reports/TaradosGP")]
        [HttpGet]
        public IQueryable<ReportTarados> TaradosGP()
        {

            // var fecha = lst.FirstOrDefault().tarado;
            return (from item in db.vw_tarados_gp

                    select new ReportTarados
                    {
                        mn = item.MN,
                        cliente = item.CLIENTE,
                        destino = item.DESTINO,
                        producto = item.PRODUCTO,
                        transportadora = item.TRANSPORTADORA,
                        placa = item.PLACA,
                        orden = item.ORDEN,
                        concepto = item.CONCEPTO,
                        bodega = item.BODEGA,
                        //tarado = item.TARADO != null ? item.TARADO.Value.ToString("yyyyMMdd HH:mm") : string.Empty,
                        tarado = item.TARADO,
                        ingresaSprb = item.INGRESA_SPRB,
                        tara = item.TARA != null ? item.TARA.Value : -1,
                        enturnado = item.ENTURNADO,
                        radicado = item.RADICADO,
                        reservaT = item.RESERVA_TURNO,
                        compartido = item.tr_cmprtdo


                    }

                    );
        }

        [Route("api/Reports/TaradosGP/{fechaInicial}/{fechaFinal}")]
        [HttpGet]
        public IEnumerable<ReportTarados> TaradosGP(string fechaInicial, string fechaFinal)
        {

            DateTime fInicial = DateTime.ParseExact(fechaInicial, "yyyy-MM-dd", System.Globalization.CultureInfo.CurrentCulture, System.Globalization.DateTimeStyles.None);
            DateTime fFinal = DateTime.ParseExact(fechaFinal, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
            fFinal = fFinal.AddDays(1);

            IEnumerable<ReportTarados> list = (from item in db.vw_tarados_gp.Where(x => SqlFunctions.DateAdd("day", 0, (x.tr_fcha_entrda)) >= fInicial && SqlFunctions.DateAdd("day", 0, x.tr_fcha_entrda) <= fFinal)
                                                   //  let convertedDate = SqlFunctions.DateName("year", item.tr_fcha_entrda) + "/" + SqlFunctions.Replicate("0", 2 - SqlFunctions.StringConvert((double)item.tr_fcha_entrda.m).TrimStart().Length) + SqlFunctions.StringConvert((double)p.CreatedDate.Month).TrimStart() + "/" + SqlFunctions.Replicate("0", 2 - SqlFunctions.DateName("dd", p.CreatedDate).Trim().Length) + SqlFunctions.DateName("dd", p.CreatedDate).Trim()
                                               select new ReportTarados
                                               {
                                                   mn = item.MN,
                                                   cliente = item.CLIENTE,
                                                   destino = item.DESTINO,
                                                   producto = item.PRODUCTO,
                                                   transportadora = item.TRANSPORTADORA,
                                                   placa = item.PLACA,
                                                   orden = item.ORDEN,
                                                   concepto = item.CONCEPTO,
                                                   bodega = item.BODEGA,
                                                   //tarado = item.TARADO != null ? item.TARADO.Value.ToString("yyyyMMdd HH:mm") : string.Empty,
                                                   tarado = item.TARADO,
                                                   ingresaSprb = item.INGRESA_SPRB,
                                                   tara = item.TARA != null ? item.TARA.Value : -1,
                                                   enturnado = item.ENTURNADO,
                                                   radicado = item.RADICADO,
                                                   reservaT = item.RESERVA_TURNO,
                                                   compartido = item.tr_cmprtdo
                                               }

                    );
            return list;

        }


        /// 
        [Route("api/Reports/TaradosPorVerificarGP/{fechaInicial}/{fechaFinal}")]
        [HttpGet]
        public IEnumerable<ReportTarados> TaradosPorVerificarGP(string fechaInicial, string fechaFinal)
        {

            DateTime fInicial = DateTime.ParseExact(fechaInicial, "yyyy-MM-dd", System.Globalization.CultureInfo.CurrentCulture, System.Globalization.DateTimeStyles.None);
            DateTime fFinal = DateTime.ParseExact(fechaFinal, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
            fFinal = fFinal.AddDays(1);

            IEnumerable<ReportTarados> list = (from item in db.vw_tarados_por_verificarGP.Where(x => SqlFunctions.DateAdd("day", 0, (x.tr_fcha_entrda)) >= fInicial && SqlFunctions.DateAdd("day", 0, x.tr_fcha_entrda) <= fFinal)
                                                   //  let convertedDate = SqlFunctions.DateName("year", item.tr_fcha_entrda) + "/" + SqlFunctions.Replicate("0", 2 - SqlFunctions.StringConvert((double)item.tr_fcha_entrda.m).TrimStart().Length) + SqlFunctions.StringConvert((double)p.CreatedDate.Month).TrimStart() + "/" + SqlFunctions.Replicate("0", 2 - SqlFunctions.DateName("dd", p.CreatedDate).Trim().Length) + SqlFunctions.DateName("dd", p.CreatedDate).Trim()
                                               select new ReportTarados
                                               {
                                                   mn = item.MN,
                                                   cliente = item.CLIENTE,
                                                   destino = item.DESTINO,
                                                   producto = item.PRODUCTO,
                                                   transportadora = item.TRANSPORTADORA,
                                                   placa = item.PLACA,
                                                   orden = item.ORDEN,
                                                   concepto = item.CONCEPTO,
                                                   bodega = item.BODEGA,
                                                   //tarado = item.TARADO != null ? item.TARADO.Value.ToString("yyyyMMdd HH:mm") : string.Empty,
                                                   tarado = item.TARADO,
                                                   ingresaSprb = item.INGRESA_SPRB,
                                                   tara = item.TARA != null ? item.TARA.Value : -1,
                                                   enturnado = item.ENTURNADO,
                                                   radicado = item.RADICADO,
                                                   reservaT = item.RESERVA_TURNO,
                                                   compartido = item.tr_cmprtdo
                                               }

                    );
            return list;

        }

        [Route("api/Reports/Destarados")]
        [HttpGet]
        public IEnumerable<ReportDestarados> Destarados()
        {

            return (from item in db.vw_destarados

                    select new ReportDestarados
                    {
                        mn = item.MN,
                        cliente = item.CLIENTE,
                        destino = item.DESTINO,
                        producto = item.PRODUCTO,
                        transportadora = item.TRANSPORTADORA,
                        placa = item.PLACA,
                        orden = item.ORDEN,
                        concepto = item.CONCEPTO,
                        bodega = item.BODEGA,
                        tarado = item.TARADO,
                        ingresaSprb = item.INGRESA_SPRB,
                        tara = item.TARA != null ? item.TARA.Value : -1,
                        enturnado = item.ENTURNADO,
                        radicado = item.RADICADO,
                        reservaT = item.RESERVA_TURNO,
                        basTara = item.BAS_DESTARA,
                        basDestara = item.BAS_DESTARA,
                        consecutivo = item.CONSECUTIVO,
                        destara = item.DESTARA.Value,
                        destarado = item.DESTARADO,
                        equipoDesc = item.EQUIPO_DESC,
                        escotilla = item.ESCOTILLA,
                        neto = item.NETO,
                        salidaSprb = item.SALIDA_SPRB,
                        visita = item.VISITA,
                        Compartido =item.compartido


                    }

                    );
        }



        [Route("api/Reports/Destarados/{fechaInicial}/{fechaFinal}")]
        [HttpGet]
        public IEnumerable<ReportDestarados> Destarados(string fechaInicial, string fechaFinal)
        {

            DateTime fInicial = DateTime.ParseExact(fechaInicial, "yyyy-MM-dd", System.Globalization.CultureInfo.CurrentCulture, System.Globalization.DateTimeStyles.None);
            DateTime fFinal = DateTime.ParseExact(fechaFinal, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
            fFinal = fFinal.AddDays(1);
            IEnumerable<ReportDestarados> list = (from item in db.vw_destarados.Where(x => SqlFunctions.DateAdd("day", 0, x.TARADO) >= fInicial && SqlFunctions.DateAdd("day", 0, x.TARADO) <= fFinal)

                                                  select new ReportDestarados
                                                  {
                                                      mn = item.MN,
                                                      cliente = item.CLIENTE,
                                                      destino = item.DESTINO,
                                                      producto = item.PRODUCTO,
                                                      transportadora = item.TRANSPORTADORA,
                                                      placa = item.PLACA,
                                                      orden = item.ORDEN,
                                                      concepto = item.CONCEPTO,
                                                      bodega = item.BODEGA,
                                                      tarado = item.TARADO,
                                                      ingresaSprb = item.INGRESA_SPRB,
                                                      tara = item.TARA != null ? item.TARA.Value : -1,
                                                      enturnado = item.ENTURNADO,
                                                      radicado = item.RADICADO,
                                                      reservaT = item.RESERVA_TURNO,
                                                      basTara = item.BAS_DESTARA,
                                                      basDestara = item.BAS_DESTARA,
                                                      consecutivo = item.CONSECUTIVO,
                                                      destara = item.DESTARA.Value,
                                                      destarado = item.DESTARADO,
                                                      equipoDesc = item.EQUIPO_DESC,
                                                      escotilla = item.ESCOTILLA,
                                                      neto = item.NETO,
                                                      salidaSprb = item.SALIDA_SPRB,
                                                      visita = item.VISITA,
                                                      Compartido = item.compartido

                                                  }

                    );
            return list;
        }


        [Route("api/Reports/Destarados/{fechaInicial}/{fechaFinal}/{tFecha}")]
        [HttpGet]
        public IEnumerable<ReportDestarados> Destarados(string fechaInicial, string fechaFinal, int tFecha)
        {


            DateTime fInicial = DateTime.ParseExact(fechaInicial, "yyyy-MM-dd", System.Globalization.CultureInfo.CurrentCulture, System.Globalization.DateTimeStyles.None);
            DateTime fFinal = DateTime.ParseExact(fechaFinal, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
            fFinal = fFinal.AddDays(1);
            tipoFecha opcionFecha = (tipoFecha)Enum.ToObject(typeof(tipoFecha), tFecha);

            IEnumerable<ReportDestarados> list = null;

            switch (opcionFecha)
            {
                #region FechaEntrada
                case
                  tipoFecha.FechaEntrada:
                    list = (from item in db.vw_destarados.Where(x => SqlFunctions.DateAdd("day", 0, x.TARADO) >= fInicial && SqlFunctions.DateAdd("day", 0, x.TARADO) <= fFinal)

                            select new ReportDestarados
                            {
                                mn = item.MN,
                                cliente = item.CLIENTE,
                                destino = item.DESTINO,
                                producto = item.PRODUCTO,
                                transportadora = item.TRANSPORTADORA,
                                placa = item.PLACA,
                                orden = item.ORDEN,
                                concepto = item.CONCEPTO,
                                bodega = item.BODEGA,
                                tarado = item.TARADO,
                                ingresaSprb = item.INGRESA_SPRB,
                                tara = item.TARA != null ? item.TARA.Value : -1,
                                enturnado = item.ENTURNADO,
                                radicado = item.RADICADO,
                                reservaT = item.RESERVA_TURNO,
                                basTara = item.BAS_DESTARA,
                                basDestara = item.BAS_DESTARA,
                                consecutivo = item.CONSECUTIVO,
                                destara = item.DESTARA.Value,
                                destarado = item.DESTARADO,
                                equipoDesc = item.EQUIPO_DESC,
                                escotilla = item.ESCOTILLA,
                                neto = item.NETO,
                                salidaSprb = item.SALIDA_SPRB,
                                visita = item.VISITA,
                                empresa = item.empresa,
                                Compartido = item.compartido


                            }

                    );
                    break;
                #endregion
                case
                #region Fecha Salida
                  tipoFecha.FechaSalida:
                    list = (from item in db.vw_destarados.Where(x => SqlFunctions.DateAdd("day", 0, x.DESTARADO) >= fInicial && SqlFunctions.DateAdd("day", 0, x.DESTARADO) <= fFinal)

                            select new ReportDestarados
                            {
                                mn = item.MN,
                                cliente = item.CLIENTE,
                                destino = item.DESTINO,
                                producto = item.PRODUCTO,
                                transportadora = item.TRANSPORTADORA,
                                placa = item.PLACA,
                                orden = item.ORDEN,
                                concepto = item.CONCEPTO,
                                bodega = item.BODEGA,
                                tarado = item.TARADO,
                                ingresaSprb = item.INGRESA_SPRB,
                                tara = item.TARA != null ? item.TARA.Value : -1,
                                enturnado = item.ENTURNADO,
                                radicado = item.RADICADO,
                                reservaT = item.RESERVA_TURNO,
                                basTara = item.BAS_DESTARA,
                                basDestara = item.BAS_DESTARA,
                                consecutivo = item.CONSECUTIVO,
                                destara = item.DESTARA.Value,
                                destarado = item.DESTARADO,
                                equipoDesc = item.EQUIPO_DESC,
                                escotilla = item.ESCOTILLA,
                                neto = item.NETO,
                                salidaSprb = item.SALIDA_SPRB,
                                visita = item.VISITA,
                                empresa = item.empresa,
                                Compartido = item.compartido

                            }

                    );
                    break;
                    #endregion Fecha Salida


            }

            return list;
        }
        [Route("api/Reports/Muelles")]
        [HttpGet]
        public IEnumerable<ReportMuelles> getMuelles()
        {

            return (from item in db.vw_muelles

                    select new ReportMuelles
                    {
                        mn = item.MN,
                        cliente = item.CLIENTE,
                        destino = item.DESTINO,
                        producto = item.PRODUCTO,
                        transportadora = item.TRANSPORTADORA,
                        placa = item.PLACA,
                        orden = item.ORDEN,
                        concepto = item.CONCEPTO,
                        bodega = item.BODEGA,
                        entrada = item.ENTRADA,
                        salida = item.SALIDA,
                        tara = item.TARA != null ? item.TARA.Value : -1,
                        consecutivo = item.CONSECUTIVO,
                        destara = item.DESTARA.Value,
                        neto = item.NETO
                    }

                    );
        }


        [Route("api/Reports/Muelles/{fechaInicial}/{fechaFinal}")]
        [HttpGet]
        public IEnumerable<ReportMuelles> getMuelles(string fechaInicial, string fechaFinal)
        {

            DateTime fInicial = DateTime.ParseExact(fechaInicial, "yyyy-MM-dd", System.Globalization.CultureInfo.CurrentCulture, System.Globalization.DateTimeStyles.None);
            DateTime fFinal = DateTime.ParseExact(fechaFinal, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
            fFinal = fFinal.AddDays(1);
            IEnumerable<ReportMuelles> list = (from item in db.vw_muelles.Where(x => x.ENTRADA >= fInicial && x.ENTRADA <= fFinal)

                                               select new ReportMuelles
                                               {
                                                   mn = item.MN,
                                                   cliente = item.CLIENTE,
                                                   destino = item.DESTINO,
                                                   producto = item.PRODUCTO,
                                                   transportadora = item.TRANSPORTADORA,
                                                   placa = item.PLACA,
                                                   orden = item.ORDEN,
                                                   concepto = item.CONCEPTO,
                                                   bodega = item.BODEGA,
                                                   entrada = item.ENTRADA,
                                                   salida = item.SALIDA,
                                                   tara = item.TARA != null ? item.TARA.Value : -1,
                                                   consecutivo = item.CONSECUTIVO,
                                                   destara = item.DESTARA.Value,
                                                   neto = item.NETO,
                                                   empresa = item.empresa
                                               }

                    );
            return list;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Route("api/Reports/TaraVolquetas")]
        [HttpGet]
        public IEnumerable<ReportTaraVolquetas> getTaraVolquetas()
        {

            return (from item in db.vw_tara_volquetas

                    select new ReportTaraVolquetas
                    {


                        transportadora = item.TRANSPORTADORA,
                        placa = item.PLACA,
                        tara = item.TARA != null ? item.TARA.Value : -1,
                        cedula = item.CEDULA,
                        conductor = item.CONDUCTOR,
                        fecha = item.FECHA,
                        pesadas = item.PESADAS
                    }

                    );
        }


        [Route("api/Reports/TaraVolquetas/{fechaInicial}/{fechaFinal}")]
        [HttpGet]
        public IEnumerable<ReportTaraVolquetas> getTaraVolquetas(string fechaInicial, string fechaFinal)
        {

            DateTime fInicial = DateTime.ParseExact(fechaInicial, "yyyy-MM-dd", System.Globalization.CultureInfo.CurrentCulture, System.Globalization.DateTimeStyles.None);
            DateTime fFinal = DateTime.ParseExact(fechaFinal, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
            fFinal = fFinal.AddDays(1);
            IEnumerable<ReportTaraVolquetas> list = (from item in db.vw_tara_volquetas.Where(x => x.FECHA >= fInicial && x.FECHA <= fFinal)

                                                     select new ReportTaraVolquetas
                                                     {


                                                         transportadora = item.TRANSPORTADORA,
                                                         placa = item.PLACA,
                                                         tara = item.TARA != null ? item.TARA.Value : -1,
                                                         cedula = item.CEDULA,
                                                         conductor = item.CONDUCTOR,
                                                         fecha = item.FECHA,
                                                         pesadas = item.PESADAS,
                                                         empresa = item.empresa
                                                     }

                    );
            return list;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Route("api/Reports/Varios")]
        [HttpGet]
        public IEnumerable<ReportVarios> getVarios()
        {

            return (from item in db.vw_varios

                    select new ReportVarios
                    {
                        deposito = item.DEPOSITO,
                        concepto = item.CONCEPTO,
                        escotilla = item.ESCOTILLA,
                        mn = item.MN,
                        producto = item.PRODUCTO,
                        bodega = item.BODEGA,
                        placa = item.PLACA,
                        consecutivo = item.CONSECUTIVO,
                        ordenInterna = item.ORDEN_INTERNA,
                        entrada = item.ENTRADA,
                        salida = item.SALIDA,
                        tara = item.TARA,
                        bruto = item.BRUTO,
                        neto = item.NETO,
                        equipoDescargue = item.EQUIPO_DESCARGUE,
                        bas_tara = item.BAS_TARA,
                        bas_destara = item.BAS_DESTARA,
                        observaciones = item.OBSERVACIONES,
                        tiCncpto = item.ti_cncpto
                    }

                    );
        }

        [Route("api/Reports/Varios/{fechaInicial}/{fechaFinal}")]
        [HttpGet]
        public IEnumerable<ReportVarios> getVarios(string fechaInicial, string fechaFinal)
        {

            DateTime fInicial = DateTime.ParseExact(fechaInicial, "yyyy-MM-dd", System.Globalization.CultureInfo.CurrentCulture, System.Globalization.DateTimeStyles.None);
            DateTime fFinal = DateTime.ParseExact(fechaFinal, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
            fFinal = fFinal.AddDays(1);
            IEnumerable<ReportVarios> list = (from item in db.vw_varios.Where(x => x.ENTRADA >= fInicial && x.ENTRADA <= fFinal)

                                              select new ReportVarios
                                              {
                                                  deposito = item.DEPOSITO,
                                                  concepto = item.CONCEPTO,
                                                  escotilla = item.ESCOTILLA,
                                                  mn = item.MN,
                                                  producto = item.PRODUCTO,
                                                  bodega = item.BODEGA,
                                                  placa = item.PLACA,
                                                  consecutivo = item.CONSECUTIVO,
                                                  ordenInterna = item.ORDEN_INTERNA,
                                                  entrada = item.ENTRADA,
                                                  salida = item.SALIDA,
                                                  tara = item.TARA,
                                                  bruto = item.BRUTO,
                                                  neto = item.NETO,
                                                  equipoDescargue = item.EQUIPO_DESCARGUE,
                                                  bas_tara = item.BAS_TARA,
                                                  bas_destara = item.BAS_DESTARA,
                                                  observaciones = item.OBSERVACIONES,
                                                  tiCncpto = item.ti_cncpto,
                                                  empresa = item.empresa
                                              }

                    );
            return list.OrderBy(x=>x.mn);
        }


        [Route("api/Reports/Cantidades")]
        [HttpGet]
        public Cantidades Cantidades()
        {

            Cantidades cantidades = new Cantidades();
            cantidades.cantidadEnturnados = db.vw_enturnados.Count();
            cantidades.cantDestarados = db.vw_destarados.Count();
            cantidades.cantMuelle = db.vw_muelles.Count();
            cantidades.cantRadicados = db.vw_radicados.Count();
            cantidades.cantReservaTurnos = db.vw_reserva_turnos.Count();
            cantidades.cantTarados = db.vw_tarados.Count();
            cantidades.cantTaraVolquetas = db.vw_tara_volquetas.Count();
            cantidades.cantVarios = db.vw_varios.Count();

            return cantidades;


        }

        [Route("api/Reports/bodegas")]
        [HttpGet]
        public IEnumerable<Bodegas> getBodegas()
        {


            return (from item in db.bdga

                    select new Bodegas
                    {
                        id = item.bo_cdgo , //,
                        nombre = item.bo_dscrpcion
                    }

                 );
        }

        [Route("api/Reports/setBodega/{orden}/{observacion}/{bodega}/{user}/{escotilla}/{empresa}/{compartido}")]
        [HttpGet, HttpPost]
        public void setBodega(string orden, string observacion, string bodega, string user, int escotilla, string empresa, int compartido)
        {

            int userId = 1;
            turnoOrden entidad = new turnoOrden();

            entidad.numero_orden = orden;
            entidad.bodega = bodega;
            entidad.id = userId;
            entidad.estado = true;
            entidad.observacion = observacion.Equals("(Ninguna)") ? "" : observacion;
            if (escotilla != -1)
                entidad.escotilla = escotilla;
            entidad.fecha_creacion = DateTime.Now;
            entidad.empresa = empresa;
            entidad.compartido = compartido;
            if (!existRow(entidad))
            {
                db.turnoOrden.Add(entidad);
                db.SaveChanges();
            }
        }

        [Route("api/Reports/setBodega/{orden}/{observacion}/{bodega}/{user}/{escotilla}/{empresa}")]
        [HttpGet, HttpPost]
        public void setBodega(string orden, string observacion, string bodega, string user, int escotilla, string empresa)
        {
            int compartido = 0;
            int userId = 1;
            turnoOrden entidad = new turnoOrden();

            entidad.numero_orden = orden;
            entidad.bodega = bodega;
            entidad.id = userId;
            entidad.estado = true;
            entidad.observacion = observacion.Equals("(Ninguna)") ? "" : observacion;
            if (escotilla != -1)
                entidad.escotilla = escotilla;
            entidad.fecha_creacion = DateTime.Now;
            entidad.empresa = empresa;
            entidad.compartido = compartido;
            if (!existRow(entidad))
            {
                db.turnoOrden.Add(entidad);

                db.SaveChanges();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="entidad"></param>
        /// <returns></returns>
        public Boolean existRow(turnoOrden entidad)
        {

            try
            {
                IQueryable<String> numeroOrden = (from turno in db.turnoOrden
                                                  where turno.numero_orden == entidad.numero_orden && turno.compartido == entidad.compartido
                                                  select turno.numero_orden
                              );

                if (numeroOrden.Count() > 0)
                    return true;
                else
                    return false;

            }

            catch (Exception e)
            {
                return true;
            }


        }

        [Route("api/Reports/taradosAsignados/{fechaInicial}/{fechaFinal}/{tFecha}")]
        [HttpGet]
        // metodo buscar
        public IEnumerable<TaradosAsignados> taradosAsignados(string fechaInicial, string fechaFinal, int tFecha)
        {

            DateTime fInicial = DateTime.ParseExact(fechaInicial, "yyyy-MM-dd", System.Globalization.CultureInfo.CurrentCulture, System.Globalization.DateTimeStyles.None);
            DateTime fFinal = DateTime.ParseExact(fechaFinal, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
            fFinal = fFinal.AddDays(1);
            tipoFecha opcionFecha = (tipoFecha)Enum.ToObject(typeof(tipoFecha), tFecha);

            IEnumerable<TaradosAsignados> list = null;

            switch (opcionFecha)
            {
                #region FechaEntrada
                case
                    tipoFecha.FechaEntrada:
                    list = (from item in db.vw_tarados_verificados.Where(x => x.or_fcha_entrda >= fInicial && x.or_fcha_entrda <= fFinal)

                            select new TaradosAsignados
                            {

                                mn = item.am_dscrpcion,
                                placa = item.or_plca,
                                producto = item.ar_nmbre,
                                bodega = item.bo_dscrpcion,
                                tarado = item.or_fcha_entrda,
                                destarado = item.fecha_salida,
                                //reservaTurno =or.or_trno_fcha,
                                bodegaAsignada = item.bodega_asignada,
                                fechaCreacion = item.fecha_creacion != null ? item.fecha_creacion.ToString() : string.Empty,
                                fechaCreacion2 = item.fecha_creacion,
                                observacion = item.observacion,
                                userId = item.user_id,
                                orden = item.or_cdgo,
                                empresa = item.empresa,
                                escotilla = item.escotilla,
                                pesoNeto=item.ti_pso_nto
                            }
                     );

                    break;
                #endregion FechaEntrada

                #region FechaSalida
                case
                    tipoFecha.FechaSalida:
                    list = (from item in db.vw_tarados_verificados.Where(x => x.fecha_salida >= fInicial && x.fecha_salida <= fFinal)
                            select new TaradosAsignados
                            {

                                mn = item.am_dscrpcion,
                                placa = item.or_plca,
                                producto = item.ar_nmbre,
                                bodega = item.bo_dscrpcion,
                                tarado = item.or_fcha_entrda,
                                destarado = item.fecha_salida,
                                //reservaTurno =or.or_trno_fcha,
                                bodegaAsignada = item.bodega_asignada,
                                fechaCreacion = item.fecha_creacion != null ? item.fecha_creacion.ToString() : string.Empty,
                                fechaCreacion2 = item.fecha_creacion,
                                observacion = item.observacion,
                                userId = item.user_id,
                                orden = item.or_cdgo,
                                empresa = item.empresa,
                                escotilla = item.escotilla,
                                pesoNeto = item.ti_pso_nto
                            }
                     );

                    break;
                #endregion FechaSalida

                #region FechaCreacion
                case
                    tipoFecha.FechaCreacion:
                    list = (from item in db.vw_tarados_verificados.Where(x => x.fecha_creacion >= fInicial && x.fecha_creacion <= fFinal)
                            select new TaradosAsignados
                            {

                                mn = item.am_dscrpcion,
                                placa = item.or_plca,
                                producto = item.ar_nmbre,
                                bodega = item.bo_dscrpcion,
                                tarado = item.or_fcha_entrda,
                                destarado = item.fecha_salida,
                                //reservaTurno =or.or_trno_fcha,
                                bodegaAsignada = item.bodega_asignada,
                                fechaCreacion = item.fecha_creacion != null ? item.fecha_creacion.ToString() : string.Empty,
                                fechaCreacion2 = item.fecha_creacion,
                                observacion = item.observacion,
                                userId = item.user_id,
                                orden = item.or_cdgo,
                                empresa = item.empresa,
                                escotilla = item.escotilla,
                                pesoNeto = item.ti_pso_nto
                            }
                    );

                    break;
                #endregion FechaCreacion

                default:
                    break;

            }




            return list;



        }


        [Route("api/Reports/taradosAsignados/{fechaInicial}/{fechaFinal}")]
        [HttpGet]
        // metodo buscar
        public IEnumerable<TaradosAsignados> taradosAsignados(string fechaInicial, string fechaFinal)
        {
            DateTime fInicial = DateTime.ParseExact(fechaInicial, "yyyy-MM-dd", System.Globalization.CultureInfo.CurrentCulture, System.Globalization.DateTimeStyles.None);
            DateTime fFinal = DateTime.ParseExact(fechaFinal, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
            fFinal = fFinal.AddDays(1);
            IEnumerable<TaradosAsignados> list = (from item in db.vw_tarados_verificados.Where(x => x.fecha_creacion >= fInicial && x.fecha_creacion <= fFinal)


                                                  select new TaradosAsignados
                                                  {

                                                      mn = item.am_dscrpcion,
                                                      placa = item.or_plca,
                                                      producto = item.ar_nmbre,
                                                      bodega = item.bo_dscrpcion,
                                                      tarado = item.or_fcha_entrda,
                                                      destarado = item.fecha_salida,
                                                      //reservaTurno =or.or_trno_fcha,
                                                      bodegaAsignada = item.bodega_asignada,
                                                      fechaCreacion = item.fecha_creacion != null ? item.fecha_creacion.ToString() : string.Empty,
                                                      fechaCreacion2 = item.fecha_creacion,
                                                      observacion = item.observacion,
                                                      userId = item.user_id,
                                                      orden = item.or_cdgo,
                                                      empresa = item.empresa,
                                                      escotilla = item.escotilla


                                                  }


                       );
            return list;


        }

        [Route("api/Reports/taradosAsignados")]
        [HttpGet]
        // metodo buscar
        public IEnumerable<TaradosAsignados> taradosAsignados()
        {
            return (from item in db.vw_tarados_verificados


                    select new TaradosAsignados
                    {

                        mn = item.am_dscrpcion,
                        placa = item.or_plca,
                        producto = item.ar_nmbre,
                        bodega = item.bo_dscrpcion,
                        tarado = item.or_trno_fcha,
                        destarado = item.fecha_salida,
                        //reservaTurno =or.or_trno_fcha,
                        bodegaAsignada = item.bodega_asignada,
                        fechaCreacion = item.fecha_creacion != null ? item.fecha_creacion.ToString() : string.Empty,
                        fechaCreacion2 = item.fecha_creacion,
                        observacion = item.observacion,
                        userId = item.user_id,
                        orden = item.or_cdgo,
                        empresa = item.empresa,
                        escotilla = item.escotilla

                    }
          );


        }

        [Route("api/Reports/saldosDisponibles/{fechaInicial}/{fechaFinal}")]
        [HttpGet]
        // metodo buscar
        public IEnumerable<saldosDisponibles> saldosDisponibles(string fechaInicial, string fechaFinal)
        {
            DateTime fInicial = DateTime.ParseExact(fechaInicial, "yyyy-MM-dd", System.Globalization.CultureInfo.CurrentCulture, System.Globalization.DateTimeStyles.None);
            DateTime fFinal = DateTime.ParseExact(fechaFinal, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
            fFinal = fFinal.AddDays(1);
            
            IEnumerable<saldosDisponibles> list = (from item in db.vw_saldos_disponibles.Where(x => x.FECHA >= fInicial && x.FECHA <= fFinal)

                  select new saldosDisponibles
                    {
                        fecha = item.FECHA,
                        hora =item.Hora,
                        deposito = item.DEPOSITO,
                        mn = item.MN,
                        cliente = item.CLIENTE,
                        producto = item.PRODUCTO,
                        BL = item.BL,
                        nacionalizado = item.NACIONALIZADO,
                        despacho = item.DESPACHADO,
                        saldoAlBl = item.SALDO_AL_BL,
                        saldoAlNac = item.SALDO_AL_NAC,
                        empresa =item.empresa

                    }
                  );
            return list;
        }

        [HttpGet]
        [Route("api/Reports/GetAllBodegas")]
        public List<int> GenerateNumbers()
        {
            return Enumerable.Range(1, 100).ToList();
        }

        [Route("api/Reports/Nacionalizados/{fechaInicial}/{fechaFinal}")]
        [HttpGet]
        public IEnumerable<ReportNacionalizacion> getNacionalizacion(string fechaInicial, string fechaFinal)
        {

            DateTime fInicial = DateTime.ParseExact(fechaInicial, "yyyy-MM-dd", System.Globalization.CultureInfo.CurrentCulture, System.Globalization.DateTimeStyles.None);
            DateTime fFinal = DateTime.ParseExact(fechaFinal, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
            fFinal = fFinal.AddDays(1);
            IEnumerable<ReportNacionalizacion> list = (from item in db.vw_Nacionalizacion.Where(x => x.Fecha_auditoria >= fInicial && x.Fecha_auditoria <= fFinal)

                                              select new ReportNacionalizacion
                                              {
                                                
                                                  consecutivo = item.consecutivo,
                                                  FechaAuditoria=item.Fecha_auditoria,
                                                  Autorizo=item.Autorizo,
                                                  NombreUsuario=item.Nombre_Usuario,
                                                  computador=item.computador,
                                                  Motivo=item.Motivo,
                                                  DetalleAuditoria = item.Detalle_auditoria,
                                                  Razon=item.Razon,
                                                  empresa = item.empresa
                                              }

                    );
            var xx= list.OrderBy(x => x.FechaAuditoria);
            return list.OrderBy(x => x.FechaAuditoria);
        }

        // Método ejemplo agregado para Diana 

        [Route("api/Reports/Test/{fechaInicial}/{fechaFinal}")]
        [HttpGet]
        public IEnumerable<ReportTest> getTest(string fechaInicial, string fechaFinal)
        {

            DateTime fInicial = DateTime.ParseExact(fechaInicial, "yyyy-MM-dd", System.Globalization.CultureInfo.CurrentCulture, System.Globalization.DateTimeStyles.None);
            DateTime fFinal = DateTime.ParseExact(fechaFinal, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
            fFinal = fFinal.AddDays(1);
            IEnumerable<ReportTest> list = (from item in db.vw_varios.Where(x => x.ENTRADA >= fInicial && x.ENTRADA <= fFinal)

                                              select new ReportTest
                                              {
                                                  deposito = item.DEPOSITO,
                                                  concepto = item.CONCEPTO,
                                                  escotilla = item.ESCOTILLA,
                                                  mn = item.MN,
                                                  producto = item.PRODUCTO,
                                                  bodega = item.BODEGA,
                                                  placa = item.PLACA,                                                 
                                                  empresa = item.empresa
                                              }
                    );
            return list.OrderBy(x => x.mn);
        }

        //
        enum tipoFecha
        {
            FechaEntrada = 1,
            FechaSalida = 2,
            FechaCreacion = 3

        };

    }
}
