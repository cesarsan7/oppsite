﻿using OppSite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace OppSite.Controllers
{
    public class cntReportesController : Controller
    {
        // GET: cntReportes
        public ActionResult enturnados()
        {
                return PartialView();
        }

        public ActionResult muelles()
        {
            return PartialView();
        }
        public ActionResult destarados()
        {
            return PartialView();
        }
        

        public ActionResult radicados()
        {
            return PartialView();
        }

        public ActionResult reservaTurnos()
        {
            return PartialView();
        }

        public ActionResult tarados()
        {
            return PartialView();
        }

        public ActionResult taraVolquetas()
        {
            return PartialView();
        }

        public ActionResult varios()
        {
            return PartialView();
        }

    }
}