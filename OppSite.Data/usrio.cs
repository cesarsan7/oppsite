//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OppSite.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class usrio
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public usrio()
        {
            this.usrio_prmso = new HashSet<usrio_prmso>();
        }
    
        public string us_cdgo { get; set; }
        public string us_nmbre { get; set; }
        public string us_clve { get; set; }
        public Nullable<bool> us_sper { get; set; }
        public Nullable<bool> us_actvo { get; set; }
        public string us_incles { get; set; }
        public Nullable<System.DateTime> us_fcha_exprcion { get; set; }
        public string us_email { get; set; }
        public string us_idntfccion { get; set; }
        public string us_clve1 { get; set; }
        public string us_clve2 { get; set; }
        public string us_clve3 { get; set; }
        public string us_clve4 { get; set; }
        public string us_clve5 { get; set; }
        public string us_clve6 { get; set; }
        public string us_clve7 { get; set; }
        public string us_clve8 { get; set; }
        public string us_clve9 { get; set; }
        public string us_clve10 { get; set; }
        public string us_clve11 { get; set; }
        public string us_clve12 { get; set; }
        public Nullable<System.DateTime> us_fcha_ultma_clve { get; set; }
        public Nullable<bool> us_ssion_blqda { get; set; }
        public Nullable<System.DateTime> us_ssion_blqda_fcha { get; set; }
        public string us_emprsa { get; set; }
        public string us_prfil { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<usrio_prmso> usrio_prmso { get; set; }
    }
}
