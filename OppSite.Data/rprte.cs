//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OppSite.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class rprte
    {
        public int re_id { get; set; }
        public string re_grpo { get; set; }
        public string re_tpo { get; set; }
        public string re_ttlo { get; set; }
        public string re_dscrpcion { get; set; }
        public string re_nmbre_plntlla { get; set; }
        public string re_sntncia_sql { get; set; }
        public string re_ttlo_rprte { get; set; }
        public string re_fltro1 { get; set; }
        public string re_fltro1_tpo { get; set; }
        public string re_fltro2 { get; set; }
        public string re_fltro2_tpo { get; set; }
        public string re_fltro3 { get; set; }
        public string re_fltro3_tpo { get; set; }
        public string re_fltro4 { get; set; }
        public string re_fltro4_tpo { get; set; }
        public string re_fltro5 { get; set; }
        public string re_fltro5_tpo { get; set; }
        public string re_fltro6 { get; set; }
        public string re_fltro6_tpo { get; set; }
        public string re_fltro7 { get; set; }
        public string re_fltro7_tpo { get; set; }
        public string re_fltro_fcha { get; set; }
        public Nullable<bool> re_landscape { get; set; }
        public string re_frmto_id { get; set; }
        public string re_fltro1_oprdor { get; set; }
        public string re_fltro2_oprdor { get; set; }
        public string re_fltro3_oprdor { get; set; }
        public string re_fltro4_oprdor { get; set; }
        public string re_fltro5_oprdor { get; set; }
        public string re_fltro6_oprdor { get; set; }
        public string re_fltro7_oprdor { get; set; }
        public string re_fltro_gnral { get; set; }
        public Nullable<bool> re_es_clnte { get; set; }
        public Nullable<bool> re_fltro_fcha_smar_dia { get; set; }
        public string re_fltro8 { get; set; }
        public string re_fltro8_tpo { get; set; }
        public string re_fltro8_oprdor { get; set; }
        public string re_fltro9 { get; set; }
        public string re_fltro9_tpo { get; set; }
        public string re_fltro9_oprdor { get; set; }
    }
}
