﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OppSite.Data
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class venus_oppEntities : DbContext
    {
        public venus_oppEntities()
            : base("name=venus_oppEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<C__MigrationHistory> C__MigrationHistory { get; set; }
        public virtual DbSet<Actividad> Actividad { get; set; }
        public virtual DbSet<actvdad_mtnve> actvdad_mtnve { get; set; }
        public virtual DbSet<arrbo_mtnve> arrbo_mtnve { get; set; }
        public virtual DbSet<arrbo_mtnve_estdo_hchos> arrbo_mtnve_estdo_hchos { get; set; }
        public virtual DbSet<artclo> artclo { get; set; }
        public virtual DbSet<audtria> audtria { get; set; }
        public virtual DbSet<autrzcion> autrzcion { get; set; }
        public virtual DbSet<bdga> bdga { get; set; }
        public virtual DbSet<bdga_crte> bdga_crte { get; set; }
        public virtual DbSet<bdga_crte_sldos> bdga_crte_sldos { get; set; }
        public virtual DbSet<bodega> bodega { get; set; }
        public virtual DbSet<bscla> bscla { get; set; }
        public virtual DbSet<Cargo> Cargo { get; set; }
        public virtual DbSet<cdad> cdad { get; set; }
        public virtual DbSet<clnte> clnte { get; set; }
        public virtual DbSet<clnte_crte> clnte_crte { get; set; }
        public virtual DbSet<clnte_dstno> clnte_dstno { get; set; }
        public virtual DbSet<clnte_frma> clnte_frma { get; set; }
        public virtual DbSet<cmprtvo> cmprtvo { get; set; }
        public virtual DbSet<cmprtvo_trnsto> cmprtvo_trnsto { get; set; }
        public virtual DbSet<cncpto> cncpto { get; set; }
        public virtual DbSet<cndcion_fctrcion> cndcion_fctrcion { get; set; }
        public virtual DbSet<cndctor> cndctor { get; set; }
        public virtual DbSet<cnfgrcion_vhclar> cnfgrcion_vhclar { get; set; }
        public virtual DbSet<cnsctvo> cnsctvo { get; set; }
        public virtual DbSet<dpsto> dpsto { get; set; }
        public virtual DbSet<dpsto_bdga> dpsto_bdga { get; set; }
        public virtual DbSet<dpsto_dclrcion_imprtcion> dpsto_dclrcion_imprtcion { get; set; }
        public virtual DbSet<dpsto_prrteo> dpsto_prrteo { get; set; }
        public virtual DbSet<dstno> dstno { get; set; }
        public virtual DbSet<empque> empque { get; set; }
        public virtual DbSet<emprsa> emprsa { get; set; }
        public virtual DbSet<entrnmnto_cpcdad> entrnmnto_cpcdad { get; set; }
        public virtual DbSet<grpo_clnte> grpo_clnte { get; set; }
        public virtual DbSet<grpo_dstno> grpo_dstno { get; set; }
        public virtual DbSet<krdex> krdex { get; set; }
        public virtual DbSet<llmda_orden> llmda_orden { get; set; }
        public virtual DbSet<lqdcion> lqdcion { get; set; }
        public virtual DbSet<lqdcion_dtlle> lqdcion_dtlle { get; set; }
        public virtual DbSet<lqdcion_intrfce> lqdcion_intrfce { get; set; }
        public virtual DbSet<mdldad_dscrgue> mdldad_dscrgue { get; set; }
        public virtual DbSet<mtnve> mtnve { get; set; }
        public virtual DbSet<mvmnto_a_prcsar> mvmnto_a_prcsar { get; set; }
        public virtual DbSet<ncnlzcion> ncnlzcion { get; set; }
        public virtual DbSet<orden> orden { get; set; }
        public virtual DbSet<orden_accion> orden_accion { get; set; }
        public virtual DbSet<Permission> Permission { get; set; }
        public virtual DbSet<prdo_fctrcion> prdo_fctrcion { get; set; }
        public virtual DbSet<prfil> prfil { get; set; }
        public virtual DbSet<prfil_prmso> prfil_prmso { get; set; }
        public virtual DbSet<prmtro> prmtro { get; set; }
        public virtual DbSet<prmtro_gnral> prmtro_gnral { get; set; }
        public virtual DbSet<prrrteo> prrrteo { get; set; }
        public virtual DbSet<prrrteo_dtlle> prrrteo_dtlle { get; set; }
        public virtual DbSet<ptio> ptio { get; set; }
        public virtual DbSet<rprte> rprte { get; set; }
        public virtual DbSet<sde> sde { get; set; }
        public virtual DbSet<slctud_rtro> slctud_rtro { get; set; }
        public virtual DbSet<slctud_rtro_autrzcion> slctud_rtro_autrzcion { get; set; }
        public virtual DbSet<slctud_rtro_lbrcion> slctud_rtro_lbrcion { get; set; }
        public virtual DbSet<slctud_rtro_trnsprtdra> slctud_rtro_trnsprtdra { get; set; }
        public virtual DbSet<slctud_rtro_vsta> slctud_rtro_vsta { get; set; }
        public virtual DbSet<sldo_fctrcion> sldo_fctrcion { get; set; }
        public virtual DbSet<srvcio_trcro> srvcio_trcro { get; set; }
        public virtual DbSet<srvcio_trnsto> srvcio_trnsto { get; set; }
        public virtual DbSet<stio_crgue> stio_crgue { get; set; }
        public virtual DbSet<tblLogin> tblLogin { get; set; }
        public virtual DbSet<tblMainMenu> tblMainMenu { get; set; }
        public virtual DbSet<tblRoles> tblRoles { get; set; }
        public virtual DbSet<tblSubMenu> tblSubMenu { get; set; }
        public virtual DbSet<tpo_artclo> tpo_artclo { get; set; }
        public virtual DbSet<tpo_bdga> tpo_bdga { get; set; }
        public virtual DbSet<tpo_dpsto> tpo_dpsto { get; set; }
        public virtual DbSet<tpo_oprcion> tpo_oprcion { get; set; }
        public virtual DbSet<tqte> tqte { get; set; }
        public virtual DbSet<tra_urbno> tra_urbno { get; set; }
        public virtual DbSet<trno_hrrio_excpcion> trno_hrrio_excpcion { get; set; }
        public virtual DbSet<trnsprtdra> trnsprtdra { get; set; }
        public virtual DbSet<trnsprtdra_frma> trnsprtdra_frma { get; set; }
        public virtual DbSet<trnsto> trnsto { get; set; }
        public virtual DbSet<tsa_dlar> tsa_dlar { get; set; }
        public virtual DbSet<turnoOrden> turnoOrden { get; set; }
        public virtual DbSet<usrio> usrio { get; set; }
        public virtual DbSet<usrio_prmso> usrio_prmso { get; set; }
        public virtual DbSet<usrio_rmto> usrio_rmto { get; set; }
        public virtual DbSet<vhclo> vhclo { get; set; }
        public virtual DbSet<vhclo_autrzdo_rdcar> vhclo_autrzdo_rdcar { get; set; }
        public virtual DbSet<vhclo_dtos> vhclo_dtos { get; set; }
        public virtual DbSet<vto_cndctor> vto_cndctor { get; set; }
        public virtual DbSet<vto_vhclo> vto_vhclo { get; set; }
        public virtual DbSet<arrbo_crte_drio> arrbo_crte_drio { get; set; }
        public virtual DbSet<arrbo_crte_drio_llvia> arrbo_crte_drio_llvia { get; set; }
        public virtual DbSet<crre> crre { get; set; }
        public virtual DbSet<dpsto_mdfccion> dpsto_mdfccion { get; set; }
        public virtual DbSet<dpsto_plca> dpsto_plca { get; set; }
        public virtual DbSet<prgrmcion_dpsto> prgrmcion_dpsto { get; set; }
        public virtual DbSet<slctud_rtro_ajste> slctud_rtro_ajste { get; set; }
        public virtual DbSet<trza> trza { get; set; }
        public virtual DbSet<vw_destarados> vw_destarados { get; set; }
        public virtual DbSet<vw_enturnados> vw_enturnados { get; set; }
        public virtual DbSet<vw_muelles> vw_muelles { get; set; }
        public virtual DbSet<vw_Nacionalizacion> vw_Nacionalizacion { get; set; }
        public virtual DbSet<vw_radicados> vw_radicados { get; set; }
        public virtual DbSet<vw_reserva_turnos> vw_reserva_turnos { get; set; }
        public virtual DbSet<vw_saldos_disponibles> vw_saldos_disponibles { get; set; }
        public virtual DbSet<vw_tara_volquetas> vw_tara_volquetas { get; set; }
        public virtual DbSet<vw_tarados> vw_tarados { get; set; }
        public virtual DbSet<vw_tarados_gp> vw_tarados_gp { get; set; }
        public virtual DbSet<vw_tarados_por_verificar> vw_tarados_por_verificar { get; set; }
        public virtual DbSet<vw_tarados_por_verificarGP> vw_tarados_por_verificarGP { get; set; }
        public virtual DbSet<vw_tarados_verificados> vw_tarados_verificados { get; set; }
        public virtual DbSet<vw_varios> vw_varios { get; set; }
        public virtual DbSet<test> test { get; set; }
    }
}
