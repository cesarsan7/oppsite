//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OppSite.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class dpsto_mdfccion
    {
        public int dm_id { get; set; }
        public string dm_dpsto { get; set; }
        public string dm_usrio_envio { get; set; }
        public string dm_usrio_rcbdo { get; set; }
        public Nullable<int> dm_cntdad_klos { get; set; }
        public Nullable<System.DateTime> dm_fcha_envio { get; set; }
        public Nullable<System.DateTime> dm_fcha_rcbdo { get; set; }
        public Nullable<System.DateTime> dm_fcha_crrdo { get; set; }
        public string dm_tpo { get; set; }
        public string dm_estdo { get; set; }
        public string dm_obsrvcion_envio { get; set; }
        public string dm_obsrvcion_rcbdo { get; set; }
    }
}
