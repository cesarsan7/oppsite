//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OppSite.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class srvcio_trcro
    {
        public Nullable<System.DateTime> st_fcha { get; set; }
        public int st_cnsctvo { get; set; }
        public string st_cncpto { get; set; }
        public string st_orden { get; set; }
        public string st_plca { get; set; }
        public string st_trnsprtdra { get; set; }
        public string st_cnfgrcion_vhclar { get; set; }
        public string st_cdla { get; set; }
        public string st_nmbre_cndctor { get; set; }
        public Nullable<int> st_pso_entrda { get; set; }
        public Nullable<int> st_pso_slda { get; set; }
        public Nullable<int> st_pso_nto { get; set; }
        public Nullable<int> st_unddes { get; set; }
        public string st_bscla_entrda { get; set; }
        public string st_bscla_slda { get; set; }
        public string st_rmlque { get; set; }
        public Nullable<System.DateTime> st_fcha_entrda { get; set; }
        public Nullable<System.DateTime> st_fcha_slda { get; set; }
        public Nullable<int> st_cnsctvo_crte { get; set; }
        public Nullable<bool> st_pso_entrda_mnual { get; set; }
        public Nullable<bool> st_pso_slda_mnual { get; set; }
        public string st_obsrvcnes { get; set; }
        public string st_usrio { get; set; }
        public Nullable<bool> st_actvo { get; set; }
        public string st_clnte { get; set; }
        public string st_artclo { get; set; }
        public Nullable<decimal> st_trfa_aplcda { get; set; }
        public Nullable<decimal> st_vlor_srvcio { get; set; }
    
        public virtual cncpto cncpto { get; set; }
        public virtual cnfgrcion_vhclar cnfgrcion_vhclar { get; set; }
        public virtual trnsprtdra trnsprtdra { get; set; }
    }
}
