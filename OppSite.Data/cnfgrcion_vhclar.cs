//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OppSite.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class cnfgrcion_vhclar
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public cnfgrcion_vhclar()
        {
            this.cmprtvo = new HashSet<cmprtvo>();
            this.cmprtvo_trnsto = new HashSet<cmprtvo_trnsto>();
            this.srvcio_trcro = new HashSet<srvcio_trcro>();
            this.srvcio_trnsto = new HashSet<srvcio_trnsto>();
            this.tqte = new HashSet<tqte>();
            this.trnsto = new HashSet<trnsto>();
        }
    
        public string cv_cdgo { get; set; }
        public string cv_dscrpcion { get; set; }
        public Nullable<int> cv_mxmo_pso_brto { get; set; }
        public Nullable<System.DateTime> cv_fcha_crcion { get; set; }
        public Nullable<decimal> cv_trfa { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cmprtvo> cmprtvo { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cmprtvo_trnsto> cmprtvo_trnsto { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<srvcio_trcro> srvcio_trcro { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<srvcio_trnsto> srvcio_trnsto { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tqte> tqte { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<trnsto> trnsto { get; set; }
    }
}
